from pywinauto.application import Application

import sys
import re
import platform
from time import sleep
from colorama import init, Fore, Back, Style
init()


print ( Back.BLUE+Fore.YELLOW+Style.BRIGHT+"\n\nKIU Checker - Script Prototipo por Efrain Valles\n\n\
Se inicio KIU, Ingrese sus datos de inicio en KIU en \n\n\
Para iniciar precione esta ventana (s) \n"+Style.RESET_ALL)

windows=platform.platform()


#Iniciando KIU
try:
    if windows.startswith("Windows-7"):
        app = Application().connect(title_re=".*KIU*", class_name="TApplication")
        dlg = app.TApplication
    elif windows.startswith("Windows-10"):
        app = Application().connect(title_re=".*KIU*", class_name="KIU")
        dlg = app.KIU
except:    
    app = Application().start("C:\Reservas\clienteres.exe")
    if windows.startswith("Windows-7"):
        dlg = app.TApplication
    elif windows.startswith("Windows-10"):
        dlg = app.KIU

try:
    if windows.startswith("Windows-7"):
        app2 = Application().connect(title_re=".*python*", class_name="ConsoleWindowClass")
    elif windows.startswith("Windows-10"):
        app2 = Application().connect(title_re="Command Prompt - python*", class_name="ConsoleWindowClass")
except:    
    print("Error en Capturar")

dlg.window()
dlg.move_window(x=None, y=None, width=600, height=600, repaint=True)

dlg2 = app2.ConsoleWindowClass
dlg2.move_window(x=700, y=0, width=600, height=600, repaint=True)


filePath = str(sys.argv[1])
list_apis=[]
list_pending=[]
list_checked=[]
print("El archivo es %s" % filePath)

breaklist = input("desea iniciar desde un orden especifico #?  :")

def checkit(listofpax):
    for i in listofpax:
        indi= listofpax.index(i)
        if i[0] == "**INF**":
            print("pax es infante, chequee manual \n %s" % (i))
            continue
        sleep(1)
        dlg.type_keys("PF-%s{ENTER}" % i[1].replace(" ","{VK_SPACE}"))
        sleep(1)
        print(Style.BRIGHT)
        print("Nombre Completo:")
        print(Back.BLUE+Fore.YELLOW+"%s/%s" % (i[1],i[2]))
        print(Style.RESET_ALL)
        print(Style.BRIGHT)
        dlg2.set_focus()
        orden = input("\nIngrese el orden ([#],[s](saltar pax), [c] si ya estaba chequeado:")
        if orden == "s":
            print("*** SALTANDO PASAJERO ***")
            sleep(1)
            list_pending.append(i)
            dlg2.set_focus()
            continue
        if orden == "c":
            print(Back.WHITE+Fore.RED+Style.BRIGHT+"*** pax ya estaba chequeado ***"+Style.RESET_ALL)
            list_checked.append(i)
            dlg2.set_focus()
        if orden == "NO":
            break
            continue
        if i[4] is not "0" and orden[0].isdigit():
            dlg.type_keys("PU%s,ST%s,%s{ENTER}" % (orden,i[3],i[4]))
            sleep(1)
            list_checked.append(i)
            dlg2.set_focus()
        elif i[4] is "0" and orden[0].isdigit():
            dlg.type_keys("PU%s,ST%s{ENTER}" % (orden,i[3]))
            list_checked.append(i)
            sleep(1)
            dlg2.set_focus()

inicio = input("desea iniciar? (s/n) :")

if inicio == "s": 
    with open(filePath) as f:
        for line in f:
            if line[0]=="1":
                bagsearch = re.search("\.W/K/(.+?) ",line)
                if bagsearch:
                    bags = bagsearch.groups()[0]
                else:
                    bags = ""
                pnrsearch = re.search("\.L/(.+?) ",line)
                if pnrsearch:
                    pnr = pnrsearch.groups()[0]
                    #print (pnr)
                else:
                    pnr=""
                seatsearch = re.search("\.R/SEAT HK1 (.+?) ",line)
                if line.startswith('.R/SEAT'):
                    seatsearch = re.search("\.R/SEAT HK1 (.+?)$",line)
                if seatsearch:
                    seat = seatsearch.groups()[0]
                    #print (seat)
                else:
                    seatline = "%s" % next(f) 
                    seatagain = re.search("\.R/SEAT HK1 (.+?) ",seatline)
                    seat=seatagain.groups()[0]

            if line.startswith(".R/DOCS"):
                param, value = line.split("DOCS HK1",1)
                value = re.split(r'/', value)
                names = "%s" % next(f)         
                if names.startswith(".RN/"):
                    namess = names.split(".RN/",1)
                elif names.startswith("1"):
                    namess = names.split("1",1)
                namessplit = re.split(r'/',namess[1])
                #print(value)
                list_apis.append([pnr,namessplit[0],namessplit[1].strip(),seat,bags,value[4],value[3],value[5]])
            elif line.startswith(".R/PSPT"):
                nextline = next(f)
                if nextline.startswith('.RN/'):
                    line = "%s%s" % (line.strip('\n'),nextline.strip('.RN/'))
                #print(line)
                param, value = line.split("PSPT HK1",1)
                value = re.split(r'/',value)
                #print(value)
                list_apis.append(["**INF**",value[3],value[4],'**INF**','**INF**',value[1],value[0],value[2]])
print (len(list_apis))

if breaklist is not "":
    del list_apis[0:int(breaklist)]
else:
    print("iniciando desde el principio")

print("\n\n Ingrese el Header del Vuelo en KIU y presione s para iniciar")
iniciar = input("iniciar (s/n): ")

if iniciar == "s":
    checkit(list_apis)

while len(list_pending) > 0:
    print("Estos son los pasajeros pendientes")
    for i in list_pending:
        print(i)
    chequear=input("desea chequearlos?")
    if chequear == "s" or chequear == "S":
	    checkit(list_pending)
